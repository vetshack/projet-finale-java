package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
    private Float result;
    private int operateur;
    Float cal = result;
    int operation = operateur;
    int n = 0, signe = 0;

    @FXML
    private Button un;

    @FXML
    private Button delete;

    @FXML
    private Button trois;

    @FXML
    private Button deux;

    @FXML
    private Button clear;

    @FXML
    private Button supp;

    @FXML
    private Button diviser;

    @FXML
    private Button multi;

    @FXML
    private Button soustraction;

    @FXML
    private Button addition;

    @FXML
    private Button egale;

    @FXML
    private Button quatre;

    @FXML
    private Button cinq;

    @FXML
    private Button six;

    @FXML
    private Button neuf;

    @FXML
    private Button sept;

    @FXML
    private Button huit;

    @FXML
    private Button zero;

    @FXML
    private Button virgule;

    @FXML
    private TextField ecrant;

    @FXML
    private Button trash;

    @FXML
    void boutton(ActionEvent event) {
        //////////////////////////////////////////////////////////// SIGNE
        if (event.getSource() == diviser){
            cal = Float.parseFloat(ecrant.getText());
            operation = 1;
            n=1;
        }else if (event.getSource() == multi){
            cal = Float.parseFloat(ecrant.getText());
            operation = 2;
            n=1;
        }else if (event.getSource() == soustraction){
            cal = Float.parseFloat(ecrant.getText());
            operation = 3;
            n=1;
        }else if (event.getSource() == addition){
            cal = Float.parseFloat(ecrant.getText());
            operation = 4;
            n=1;
        }

        //////////////////////////////////////////////////////////// RESULT
        else if (event.getSource() == egale){
            Float fin;
            if(ecrant.getText() == "") {
                ecrant.setText("");
            }
            else if (operation == -1){
                ecrant.setText(ecrant.getText());
            }
            else {

                Float operation2 = Float.parseFloat(ecrant.getText());
                System.out.println(operation+" "+cal);
                switch (operation){
                    case 1 : //division

                        ///////////////////////////////             gestion du spam égale
                        if(n>=1){
                            fin = operation2/cal;
                            ////////                                gestion d'affichage d'un entier, ou décimaux
                            isInt(String.valueOf(fin),fin);
                            ////////
                        }else{
                            fin = cal/operation2;
                            ////////
                            isInt(String.valueOf(fin),fin);
                            ////////
                        }
                        n++;
                        if(n==1){
                            cal = operation2;
                        }
                        ///////////////////////////////

                        break;
                    case 2 ://multi
                        if(n>=1){
                            fin = operation2*cal;
                            isInt(String.valueOf(fin),fin);
                        }else{
                            fin = cal*operation2;
                            isInt(String.valueOf(fin),fin);
                        }
                        n++;
                        if(n==1){
                            cal = operation2;
                        }
                        break;
                    case 3 ://soustraction
                        if(n>=1){
                            fin = operation2-cal;
                            isInt(String.valueOf(fin),fin);
                        }else{
                            fin = cal-operation2;
                            isInt(String.valueOf(fin),fin);
                        }
                        n++;
                        if(n==1){
                            cal = operation2;
                        }
                        break;
                    case 4 ://addition
                        if(n>=1){
                            fin = operation2+cal;
                            isInt(String.valueOf(fin),fin);
                        }else{
                            fin = cal+operation2;
                            isInt(String.valueOf(fin),fin);
                        }
                        n++;
                        if(n==1){
                            cal = operation2;
                        }
                        break;
                }
            }

        }else{
            if(n>=1) {
                ecrant.setText("");
                n = 0;
            }
            if(event.getSource() == un ) {
                ecrant.setText(ecrant.getText() + "1");
            }else if (event.getSource() == deux ) {
                ecrant.setText(ecrant.getText() + "2");
            }else if (event.getSource() == trois ) {
                ecrant.setText(ecrant.getText() + "3");
            }else if (event.getSource() == quatre ) {
                ecrant.setText(ecrant.getText() + "4");
            }else if (event.getSource() == cinq ) {
                ecrant.setText(ecrant.getText() + "5");
            }else if (event.getSource() == six ) {
                ecrant.setText(ecrant.getText() + "6");
            }else if (event.getSource() == sept ) {
                ecrant.setText(ecrant.getText() + "7");
            }else if (event.getSource() == huit ) {
                ecrant.setText(ecrant.getText() + "8");
            }else if (event.getSource() == neuf ) {
                ecrant.setText(ecrant.getText() + "9");
            }else if (event.getSource() == zero ){
                ecrant.setText(ecrant.getText() + "0");
            }else if (event.getSource() == virgule) {
                ecrant.setText(ecrant.getText() + ".");
            }else if (event.getSource() == trash) {
                ecrant.setText("");
            }
            else if (event.getSource() == clear) {
                ecrant.setText("");
            }else if (event.getSource() == supp) {
                String precedent = ecrant.getText();
                int taille = precedent.length();
                if(taille == 1){
                    ecrant.setText("");
                }else{
                    ecrant.setText("");
                    GestionPrecedent(taille,precedent);
                }
            }
        }
    }

    @FXML
    public void GestionPrecedent(int taille, String precedent){
        for(int i=0; i<taille-1; i++) {
            ecrant.setText(ecrant.getText() + String.valueOf(precedent.charAt(i)));
        }
    }

    @FXML
    public void isInt(String valeur, float fin) {
        if ((valeur.charAt(valeur.length() - 2) == '.') && (valeur.charAt(valeur.length() - 1) == '0')) {
            ecrant.setText(String.valueOf((int) (Math.round(fin))));
        } else {
            ecrant.setText(String.valueOf(fin));
        }
    }
}


